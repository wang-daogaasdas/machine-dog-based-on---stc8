#include "Ultrasonic.h"

void Ultrasonic_init() {
    Ultrasonic_GPIO_init();
    ECHO = 0;
    TRIG = 0;
}

void Delay10us()		//@24.000MHz
{
    unsigned char data i;

    i = 78;
    while (--i);
}

char Ultrasonic_get_distance(float *distance) {
    float dis;
    u16 cnt_10us;
    // 1、使用TRIG触发测距，至少给一个时长为10us的高电平
    TRIG = 1;
    Delay10us();
    Delay10us();
    TRIG = 0;

    // 2、等待194us左右，ECHO被拉高，如果未被拉高则return 1；
    cnt_10us = 0;
    while(ECHO == 0 && cnt_10us < 30) {
        Delay10us();
        cnt_10us++;
    }
    if(cnt_10us * 10 >= 300) {
        printf("ECHO拉高超时: %d us，退出该模块！\n",(int)(cnt_10us * 10));
        return -1;
    }

    // 3、记录ECHO从上升沿到下降沿的间隔时间
    cnt_10us = 0;
    while(ECHO == 1) {
        Delay10us();
        cnt_10us++;
    }
    printf("ECHO--> %.2f ms \n",cnt_10us * 0.01);
    // 4、将时间根据声速转化为距离 CM
    dis = 34 * cnt_10us * 0.01 * 0.5;
    *distance = dis;
    if(dis < 2) {								//距离太短，数值不准确
        return 1;
    } else if(dis > 400) {				//距离太大，数值不准确
        return 2;
    }
    return 0;
}