#ifndef __BATTERY_H__
#define __BATTERY_H__
#include "GPIO.h"

#define Battery_GPIO_init()    P0_MODE_IN_HIZ(GPIO_Pin_1)
#define ADC_CHANNEL 					ADC_CH9

void Battery_init();

void Battery_get_voltage(float *voltage);

#endif