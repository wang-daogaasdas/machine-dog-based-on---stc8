#include "Config.h"
#include "GPIO.h"
#include "Delay.h"
#include "UART.h"
#include "Exti.h"
#include "STC8H_PWM.h"
#include "spider_servo.h"

#define TASK_UART1	1


/******************** INT配置 ********************/
void Exti_config(void)
{
    EXTI_InitTypeDef	Exti_InitStructure;							//结构定义
    Exti_InitStructure.EXTI_Mode      = EXT_MODE_RiseFall;//中断模式,   EXT_MODE_RiseFall,EXT_MODE_Fall
    Ext_Inilize(EXT_INT0,&Exti_InitStructure);				//初始化
    NVIC_INT0_Init(ENABLE,Priority_0);		//中断使能, ENABLE/DISABLE; 优先级(低到高) Priority_0,Priority_1,Priority_2,Priority_3
}

void UART_config(void) {
    // >>> 记得添加 NVIC.c, UART.c, UART_Isr.c <<<
    COMx_InitDefine		COMx_InitStructure;								//结构定义
    COMx_InitStructure.UART_Mode      = UART_8bit_BRTx;	//模式, UART_ShiftRight,UART_8bit_BRTx,UART_9bit,UART_9bit_BRTx
    COMx_InitStructure.UART_BRT_Use   = BRT_Timer1;			//选择波特率发生器, BRT_Timer1, BRT_Timer2 (注意: 串口2固定使用BRT_Timer2)
    COMx_InitStructure.UART_BaudRate  = 115200ul;				//波特率, 一般 110 ~ 115200
    COMx_InitStructure.UART_RxEnable  = ENABLE;					//接收允许,   ENABLE或DISABLE
    COMx_InitStructure.BaudRateDouble = DISABLE;				//波特率加倍, ENABLE或DISABLE
    UART_Configuration(UART1, &COMx_InitStructure);			//初始化串口1 UART1,UART2,UART3,UART4

    NVIC_UART1_Init(ENABLE,Priority_1);		//中断使能, ENABLE/DISABLE; 优先级(低到高) Priority_0,Priority_1,Priority_2,Priority_3
    UART1_SW(UART1_SW_P30_P31);						// 引脚选择, UART1_SW_P30_P31,UART1_SW_P36_P37,UART1_SW_P16_P17,UART1_SW_P43_P44
}
//  -----               -----
// |PWM5 |             |PWM6|
// |IO00 |             |IO54|
//  ----- -----   ----- -----
//       |PWM1 | |PWM2 |
//       |IO20 | |I23  |
//        -----   -----
//       |PWM7 | |PWM4 |
//       |IO22 | |IO16 |
//  ----- -----   ----- -----
// |PWM3 |             |PWM8 |
// |IO24 |             |IO34 |
//  -----               -----
void ext_int0_call() {
		static int flag = 0;
    delay_ms(10);
    if(P32) {
        // 延时10ms后，依然低电平，才执行。消除抖动
        return;
    }
    // 修改占空比 0 -> 100
		Spider_servo_update(flag);
		
		flag++;
		if(flag == 2){
			flag = 0;
		}
}

void sys_init() {
    EAXSFR();

    UART_config();
    Exti_config();
    Spider_servo_init();

    printf("init complete\n");

    EA = 1;
}

void task_main() _task_ 0 {

    sys_init();

    os_create_task(TASK_UART1);

    os_delete_task(0);
}

void on_uart1_recv() {
    u8 i;
    for(i=0; i<COM1.RX_Cnt; i++)	{
        // RX1_Buffer[i]存的是接收的数据，写出用 TX1_write2buff

    }
}

void task1() _task_ TASK_UART1 {


    while(1) {
        if(COM1.RX_TimeOut > 0) {
            //超时计数
            if(--COM1.RX_TimeOut == 0) {
                if(COM1.RX_Cnt > 0) {
                    on_uart1_recv();
                }
                COM1.RX_Cnt = 0;
            }
        }
        os_wait2(K_TMO, 2);
    }

}