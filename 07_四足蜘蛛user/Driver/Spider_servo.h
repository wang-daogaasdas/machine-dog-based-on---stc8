#ifndef __SPIDER_SERVO_H
#define __SPIDER_SERVO_H

#include "Config.h"

#define COL 9

void Spider_servo_init();
void Spider_servo_update(int action);


#endif