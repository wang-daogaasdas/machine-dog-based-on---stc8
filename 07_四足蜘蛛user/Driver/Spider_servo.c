#include "spider_servo.h"
#include "STC8H_PWM.h"
#include "GPIO.h"
#include "Delay.h"

// 分频系数：可以是1~65535中的任意值
#define Prescaler 10
// 频率
#define PREQ			50
// 保证分母 >= 367

#define PERIOD (MAIN_Fosc / (PREQ * Prescaler))

//  -----               -----
// |PWM5 |             |PWM6|
// |00  0|             |54 3|
//  ----- -----   ----- -----
//       |PWM1 | |PWM2 |
//       |20  1| |23  2|
//        -----   -----
//       |PWM7 | |PWM4 |
//       |22  5| |16  6|
//  ----- -----   ----- -----
// |PWM3 |             |PWM8 |
// |24  4|             |34  7|
//  -----               -----
int machine_stand_step = 3;
//站立
int machine_stand[][COL] = {
    {40,90,60,140,158,60,70,40, 250},

    {10,120,35,170,180,25,110,10, 250},
    {40,90,60,140,158,60,70,40, 250},
} ;

//招手  {10,120,35,170,180,25,110,10, 250},
//int machine_default_step = 12;
//int machine_default[][COL] = {
//    {40,90,60,140,158,60,70,40, 250},

//		{10,120,35,170,180,25,110,10, 250},

//    {40,90,70,30,110,60,70,40, 250},
//    {40,90,70,140,110,60,70,40, 250},
//    {40,90,70,30,110,60,70,40, 250},
//    {40,90,70,140,110,60,70,40, 250},

//    {10,120,35,170,180,25,110,10, 250},

//    {20,80,60,140,150,60,70,75, 250},
//    {160,80,60,140,150,60,70,75, 250},
//    {20,80,60,140,150,60,70,75, 250},
//    {160,80,60,140,150,60,70,75, 250},

//    {40,90,60,140,158,60,70,40, 250},
//} ;

////游泳  {10,120,35,170,180,25,110,10, 250},
//int machine_default_step = 12;
//int machine_default[][COL] = {
//    {40,90,60,140,158,60,70,40, 250},

//    {10,120,35,170,180,25,110,10, 250},

//    {110,120,30,80,105,25,105,100, 250},
//    {110,23,130,80,105,115,15,100, 250},
//    {110,120,30,80,105,25,105,100, 250},
//    {110,23,130,80,105,115,15,100, 250},
//    {110,120,30,80,105,25,105,100, 250},
//    {110,23,130,80,105,115,15,100, 250},
//    {110,120,30,80,105,25,105,100, 250},
//    {110,23,130,80,105,115,15,100, 250},

//    {10,120,35,170,180,25,110,10, 250},
//    {40,90,60,140,158,60,70,40, 250},
//} ;

//运动  {10,120,35,170,180,25,110,10, 250},
//int machine_default_step = 12;
//int machine_default[][COL] = {
//    {40,90,60,140,158,60,70,40, 250},
//    {10,120,35,170,180,25,110,10, 250},

//    {100,120,35,160,120,25,100,15, 250},
//    {10,120,35,105,180,25,100,75, 250},
//    {100,120,35,160,120,25,100,15, 250},
//    {10,120,35,105,180,25,100,75, 250},
//    {100,120,35,160,120,25,100,15, 250},
//    {10,120,35,105,180,25,100,75, 250},
//    {100,120,35,160,120,25,100,15, 250},
//    {10,120,35,105,180,25,100,75, 250},

//    {10,120,35,170,180,25,110,10, 250},
//    {40,90,60,140,158,60,70,40, 250},

//} ;

//运动  {10,120,35,170,180,25,110,10, 250},
int machine_default_step = 18;
int machine_default[][COL] = {
    {40,90,60,140,158,60,70,40, 250},
    {20,120,35,170,180,25,110,10, 250},

    {40,30,35,140,180,25,135,30, 250},
    {20,120,120,155,170,0,110,10, 250},

    {40,30,35,155,170,80,135,30, 250},
    {20,120,120,155,170,0,110,10, 250},

    {40,30,35,155,170,80,135,30, 250},
    {20,120,120,155,170,0,110,10, 250},
    {40,30,35,155,170,80,135,30, 250},
    {20,120,120,155,170,0,110,10, 250},
    {40,30,35,155,170,80,135,30, 250},
    {20,120,120,155,170,0,110,10, 250},
    {40,30,35,155,170,80,135,30, 250},
    {20,120,120,155,170,0,110,10, 250},
    {40,30,35,155,170,80,135,30, 250},
    {20,120,120,155,170,0,110,10, 250},
    {20,120,35,170,180,25,110,10, 250},
    {40,90,60,140,158,60,70,40, 250},

} ;

//运动  {10,120,35,170,180,25,110,10, 250},
//int machine_default_step = 5;
//int machine_default[][COL] = {
//    {40,90,60,140,158,60,70,40, 250},
//    {20,120,35,170,180,25,110,10, 250},

//    {40,30,35,170,180,25,30,30, 250},
//    {20,30,35,170,180,25,30,10, 250},
//		
//    {20,120,110,140,160,100,110,10, 250},



//} ;

static void GPIO_config(void) {
    GPIO_InitTypeDef	GPIO_InitStructure;		//结构定义
    GPIO_InitStructure.Pin  = GPIO_Pin_0;		//指定要初始化的IO,
    GPIO_InitStructure.Mode = GPIO_OUT_PP;	//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
    GPIO_Inilize(GPIO_P0, &GPIO_InitStructure);//初始化

    GPIO_InitStructure.Pin  = GPIO_Pin_6;		//指定要初始化的IO,
    GPIO_InitStructure.Mode = GPIO_OUT_PP;	//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
    GPIO_Inilize(GPIO_P1, &GPIO_InitStructure);//初始化

    GPIO_InitStructure.Pin  = GPIO_Pin_0 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4;		//指定要初始化的IO,
    GPIO_InitStructure.Mode = GPIO_OUT_PP;	//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
    GPIO_Inilize(GPIO_P2, &GPIO_InitStructure);//初始化

    GPIO_InitStructure.Pin  = GPIO_Pin_4;		//指定要初始化的IO,
    GPIO_InitStructure.Mode = GPIO_OUT_PP;	//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
    GPIO_Inilize(GPIO_P3, &GPIO_InitStructure);//初始化

    GPIO_InitStructure.Pin  = GPIO_Pin_4;		//指定要初始化的IO,
    GPIO_InitStructure.Mode = GPIO_OUT_PP;	//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
    GPIO_Inilize(GPIO_P5, &GPIO_InitStructure);//初始化
}

static void PWM_config(void) {
    PWMx_InitDefine		PWMx_InitStructure;

    // 配置PWM1
    PWMx_InitStructure.PWM_Mode    		= CCMRn_PWM_MODE1;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
    PWMx_InitStructure.PWM_Duty    		= 0;	//PWM占空比时间, 0~Period
    PWMx_InitStructure.PWM_EnoSelect	= ENO1P;	//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
    PWM_Configuration(PWM1, &PWMx_InitStructure);			//初始化PWM
    // 配置PWM2
    PWMx_InitStructure.PWM_Mode    		= CCMRn_PWM_MODE1;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
    PWMx_InitStructure.PWM_Duty    		= 0;	//PWM占空比时间, 0~Period
    PWMx_InitStructure.PWM_EnoSelect    = ENO2N;	//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
    PWM_Configuration(PWM2, &PWMx_InitStructure);			//初始化PWM
    // 配置PWM3
    PWMx_InitStructure.PWM_Mode    		= CCMRn_PWM_MODE1;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
    PWMx_InitStructure.PWM_Duty    		= 0;	//PWM占空比时间, 0~Period
    PWMx_InitStructure.PWM_EnoSelect    = ENO3P;	//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
    PWM_Configuration(PWM3, &PWMx_InitStructure);			//初始化PWM
    // 配置PWM4
    PWMx_InitStructure.PWM_Mode    		= CCMRn_PWM_MODE1;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
    PWMx_InitStructure.PWM_Duty    		= 0;	//PWM占空比时间, 0~Period
    PWMx_InitStructure.PWM_EnoSelect    = ENO4P;	//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
    PWM_Configuration(PWM4, &PWMx_InitStructure);			//初始化PWM

    // 配置PWM5
    PWMx_InitStructure.PWM_Mode    		= CCMRn_PWM_MODE1;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
    PWMx_InitStructure.PWM_Duty    		= 0;	//PWM占空比时间, 0~Period
    PWMx_InitStructure.PWM_EnoSelect    = ENO5P;			//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
    PWM_Configuration(PWM5, &PWMx_InitStructure);			//初始化PWM,  PWMA,PWMB
    // 配置PWM6
    PWMx_InitStructure.PWM_Mode    		= CCMRn_PWM_MODE1;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
    PWMx_InitStructure.PWM_Duty    		= 0;	//PWM占空比时间, 0~Period
    PWMx_InitStructure.PWM_EnoSelect    = ENO6P;			//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
    PWM_Configuration(PWM6, &PWMx_InitStructure);			//初始化PWM,  PWMA,PWMB
    // 配置PWM7
    PWMx_InitStructure.PWM_Mode    		= CCMRn_PWM_MODE1;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
    PWMx_InitStructure.PWM_Duty    		= 0;	//PWM占空比时间, 0~Period
    PWMx_InitStructure.PWM_EnoSelect    = ENO7P;			//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
    PWM_Configuration(PWM7, &PWMx_InitStructure);			//初始化PWM,  PWMA,PWMB
    // 配置PWM8
    PWMx_InitStructure.PWM_Mode    		= CCMRn_PWM_MODE1;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
    PWMx_InitStructure.PWM_Duty    		= 0;	//PWM占空比时间, 0~Period
    PWMx_InitStructure.PWM_EnoSelect    = ENO8P;			//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
    PWM_Configuration(PWM8, &PWMx_InitStructure);			//初始化PWM,  PWMA,PWMB

    // 配置PWMB
    PWMx_InitStructure.PWM_Period   = PERIOD - 1;			//周期时间,   0~65535
    PWMx_InitStructure.PWM_DeadTime = 0;					//死区发生器设置, 0~255
    PWMx_InitStructure.PWM_MainOutEnable= ENABLE;			//主输出使能, ENABLE,DISABLE
    PWMx_InitStructure.PWM_CEN_Enable   = ENABLE;			//使能计数器, ENABLE,DISABLE
    PWM_Configuration(PWMA, &PWMx_InitStructure);			//初始化PWM通用寄存器,  PWMA,PWMB
    PWMA_Prescaler(Prescaler - 1);

    // 配置PWMB
    PWMx_InitStructure.PWM_Period   = PERIOD - 1;			//周期时间,   0~65535
    PWMx_InitStructure.PWM_DeadTime = 0;					//死区发生器设置, 0~255
    PWMx_InitStructure.PWM_MainOutEnable= ENABLE;			//主输出使能, ENABLE,DISABLE
    PWMx_InitStructure.PWM_CEN_Enable   = ENABLE;			//使能计数器, ENABLE,DISABLE
    PWM_Configuration(PWMB, &PWMx_InitStructure);			//初始化PWM通用寄存器,  PWMA,PWMB
    PWMB_Prescaler(Prescaler - 1);

    // 切换PWM通道
    PWM1_SW(PWM1_SW_P20_P21);			//PWM1_SW_P10_P11,PWM1_SW_P20_P21,PWM1_SW_P60_P61
    PWM2_SW(PWM2_SW_P22_P23);			//PWM2_SW_P12_P13,PWM2_SW_P22_P23,PWM2_SW_P62_P63
    PWM3_SW(PWM3_SW_P24_P25);			//PWM3_SW_P14_P15,PWM3_SW_P24_P25,PWM3_SW_P64_P65
    PWM4_SW(PWM4_SW_P16_P17);			//PWM4_SW_P16_P17,PWM4_SW_P26_P27,PWM4_SW_P66_P67,PWM4_SW_P34_P33

    PWM5_SW(PWM5_SW_P00);					//PWM5_SW_P20,PWM5_SW_P17,PWM5_SW_P00,PWM5_SW_P74
    PWM6_SW(PWM6_SW_P54);					//PWM6_SW_P21,PWM6_SW_P54,PWM6_SW_P01,PWM6_SW_P75
    PWM7_SW(PWM7_SW_P22);					//PWM7_SW_P22,PWM7_SW_P33,PWM7_SW_P02,PWM7_SW_P76
    PWM8_SW(PWM8_SW_P34);					//PWM8_SW_P23,PWM8_SW_P34,PWM8_SW_P03,PWM8_SW_P77

    // 初始化PWMB的中断
    NVIC_PWM_Init(PWMA,DISABLE,Priority_0);
    NVIC_PWM_Init(PWMB,DISABLE,Priority_0);
}

void Spider_servo_init() {
    GPIO_config();
    PWM_config();
}
#define ANGLE_TO_DUTY(x)		(500 + (x * 2000.0f / 180.0f));
PWMx_Duty duty;
void update_dutys(int* action) {
// 设置占空比
    duty.PWM1_Duty = PERIOD * action[1] / 20000; 		//P20 左上大腿
    duty.PWM2_Duty = PERIOD * action[2] / 20000;		//P23 右上大腿
    duty.PWM3_Duty = PERIOD * action[4] / 20000;		//P24 左下小腿
    duty.PWM4_Duty = PERIOD * action[6] / 20000;		//P16 右下大腿
    UpdatePwm(PWMA, &duty);

    delay_ms(100);

    duty.PWM5_Duty = PERIOD * action[0] / 20000;		//P00 左上小腿
    duty.PWM6_Duty = PERIOD * action[3] / 20000;		//P54 右上小腿
    duty.PWM7_Duty = PERIOD * action[5] / 20000;		//P22 左下大腿
    duty.PWM8_Duty = PERIOD * action[7] / 20000;		//P34 右下小腿
    UpdatePwm(PWMB, &duty);
}

void servo_update(int action_angles_arr[][COL], int action_step) {
    int i,j;
    int action_dutys[COL - 1] = {0};

    for (i = 0; i < action_step; i++) {
        int* action_angles = action_angles_arr[i];
        int delay = action_angles[COL - 1];
        for (j = 0; j < COL - 1; j++) {
            action_dutys[j] = ANGLE_TO_DUTY(action_angles[j]);
        }

        update_dutys(action_dutys);
        delay_X_ms(delay);
    }
}
void Spider_servo_update(int action) {
    switch(action) {
    case 0:
        servo_update(machine_default, machine_default_step);
        break;
    case 1:
        servo_update(machine_stand, machine_stand_step);
        break;
    }
}

