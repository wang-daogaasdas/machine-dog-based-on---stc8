#ifndef __ULTRASONIC_H__
#define __ULTRASONIC_H__
#include "GPIO.h"

#define Ultrasonic_GPIO_init()  P1_MODE_IO_PU(GPIO_Pin_3) \
																P4_MODE_IN_HIZ(GPIO_Pin_0)\

#define ECHO P40
#define TRIG P13

void Ultrasonic_init();

char Ultrasonic_get_distance(float *distance);

#endif