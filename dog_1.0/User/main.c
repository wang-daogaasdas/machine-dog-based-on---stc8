#include "Config.h"
#include "GPIO.h"
#include "oled.h"
#include "bmp.h"
#include "I2C.h"
#include "UART.h"
#include "Ultrasonic.h"
#include "Battery.h"


//全部宏定义
#define Task_creation 0
#define IIC_OLED      1
#define BT_UART1      2
#define BT_UART2      3
#define Ultrasonic    4
#define ADC						5

#define WHEEL_X				2
#define WHEEL_Y 			3

u8 ultrasonic_enable = DISABLE;	// 测距功能, 默认未启用

float voltage = -1.0f;

void GPIO_config() {

    //IIC屏幕初始化
    P3_MODE_OUT_OD(GPIO_Pin_2 | GPIO_Pin_3);

    //蓝牙初始化  EN使能引脚P03  UART1、UART2通讯引脚P10、P11
    P0_MODE_IO_PU(GPIO_Pin_3);
    P1_MODE_IO_PU(GPIO_Pin_0 | GPIO_Pin_1);

}

/****************  I2C初始化函数 *****************/
void	I2C_config(void)
{
    I2C_InitTypeDef		I2C_InitStructure;

    I2C_InitStructure.I2C_Mode      = I2C_Mode_Master;	//主从选择   I2C_Mode_Master, I2C_Mode_Slave
    I2C_InitStructure.I2C_Enable    = ENABLE;			//I2C功能使能,   ENABLE, DISABLE
    I2C_InitStructure.I2C_MS_WDTA   = DISABLE;			//主机使能自动发送,  ENABLE, DISABLE
    I2C_InitStructure.I2C_Speed     = 13;				//总线速度=Fosc/2/(Speed*2+4),      0~63
    // 400k, 24M => 13
    I2C_Init(&I2C_InitStructure);
    NVIC_I2C_Init(I2C_Mode_Master,DISABLE,Priority_0);	//主从模式, I2C_Mode_Master, I2C_Mode_Slave; 中断使能, ENABLE/DISABLE; 优先级(低到高) Priority_0,Priority_1,Priority_2,Priority_3

    I2C_SW(I2C_P33_P32);					//I2C_P14_P15,I2C_P24_P25,I2C_P33_P32
}

void UART_config(void) {
    // >>> 记得添加 NVIC.c, UART.c, UART_Isr.c <<<
    COMx_InitDefine		COMx_InitStructure;					//结构定义
    COMx_InitStructure.UART_Mode      = UART_8bit_BRTx;	//模式, UART_ShiftRight,UART_8bit_BRTx,UART_9bit,UART_9bit_BRTx
    COMx_InitStructure.UART_BRT_Use   = BRT_Timer1;			//选择波特率发生器, BRT_Timer1, BRT_Timer2 (注意: 串口2固定使用BRT_Timer2)
    COMx_InitStructure.UART_BaudRate  = 115200ul;			//波特率, 一般 110 ~ 115200
    COMx_InitStructure.UART_RxEnable  = ENABLE;				//接收允许,   ENABLE或DISABLE
    COMx_InitStructure.BaudRateDouble = DISABLE;			//波特率加倍, ENABLE或DISABLE
    UART_Configuration(UART1, &COMx_InitStructure);		//初始化串口1 UART1,UART2,UART3,UART4

    NVIC_UART1_Init(ENABLE,Priority_1);		//中断使能, ENABLE/DISABLE; 优先级(低到高) Priority_0,Priority_1,Priority_2,Priority_3
    UART1_SW(UART1_SW_P30_P31);		// 引脚选择, UART1_SW_P30_P31,UART1_SW_P36_P37,UART1_SW_P16_P17,UART1_SW_P43_P44

    COMx_InitStructure.UART_Mode      = UART_8bit_BRTx;	//模式, UART_ShiftRight,UART_8bit_BRTx,UART_9bit,UART_9bit_BRTx
    COMx_InitStructure.UART_BRT_Use   = BRT_Timer2;			//选择波特率发生器, BRT_Timer1, BRT_Timer2 (注意: 串口2固定使用BRT_Timer2)
    COMx_InitStructure.UART_BaudRate  = 9600ul;			//波特率, 一般 110 ~ 115200
    COMx_InitStructure.UART_RxEnable  = ENABLE;				//接收允许,   ENABLE或DISABLE
    COMx_InitStructure.BaudRateDouble = DISABLE;			//波特率加倍, ENABLE或DISABLE
    UART_Configuration(UART2, &COMx_InitStructure);		//初始化串口1 UART1,UART2,UART3,UART4

    NVIC_UART2_Init(ENABLE,Priority_1);		//中断使能, ENABLE/DISABLE; 优先级(低到高) Priority_0,Priority_1,Priority_2,Priority_3
    UART2_SW(UART2_SW_P10_P11);		// 引脚选择, UART2_SW_P10_P11,UART2_SW_P46_P47
}

//初始化模块
void sys_init() {
    EAXSFR();

    GPIO_config();
    I2C_config();
    Ultrasonic_init();
    Battery_init();
    UART_config();

    EA = 1;
}

//任务创建模块
void start_main() _task_ Task_creation{

    sys_init();

    os_create_task(IIC_OLED);
    os_create_task(BT_UART1);
    os_create_task(BT_UART2);
    os_create_task(ADC);

    os_delete_task(Task_creation);

}

//任务1：	IIC_OLED 屏幕模块
void task_OLED() _task_ IIC_OLED{
    OLED_config();
    OLED_ColorTurn(0);//0正常显示，1 反色显示
    OLED_DisplayTurn(0);//0正常显示 1 屏幕翻转显示
    while(1) {
        OLED_DrawBMP(0,0,128,64,BMP_on);
    }
}

//任务2： 蓝牙通讯模块 接收PC从UART1传过来的消息, 通过UART2转发给蓝牙模块
void task_UART1() _task_ BT_UART1{
    u8 i;
    while(1) {
        if(COM1.RX_TimeOut > 0) {
            //超时计数
            if(--COM1.RX_TimeOut == 0) {
                if(COM1.RX_Cnt > 0) {
                    for(i=0; i<COM1.RX_Cnt; i++)	{
                        // RX1_Buffer[i]存的是接收的数据，写出用 TX1_write2buff
                        // TODO: 做具体的逻辑 on_uart1_recv
                        TX2_write2buff(RX1_Buffer[i]);
                    }
                }
                COM1.RX_Cnt = 0;
            }
        }
        os_wait2(K_TMO, 2);
    }
}

//任务2：处理蓝牙通讯传送的数据，打开相应功能
// B:DD 77 00 00 00 01 00 00
// C:DD 77 00 00 00 00 01 00
// D:DD 77 00 00 00 00 00 01
// A:DD 77 00 00 01 00 00 00

void do_control() {

    //判断数据是否规范
    if(COM2.RX_Cnt < 8) {
        return;
    }
    if(RX2_Buffer[0] != 0xDD || RX2_Buffer[1] != 0x77) {
        return;
    }
    //第五位0x01 代表B被按下 启用测距功能
    if(RX2_Buffer[5] == 0x01) {
        if(ultrasonic_enable) {
            os_create_task(Ultrasonic);
        } else {
            os_delete_task(Ultrasonic);
        }
        ultrasonic_enable = !ultrasonic_enable;
    }
    //第六位0x01 代表C被按下
    if(RX2_Buffer[6] == 0x01) {
        printf("输出C");
    }
    //第七位0x01 代表D被按下
    if(RX2_Buffer[6] == 0x01) {
        printf("输出D");
    }
    //第四位0x01 代表A被按下 启动巡线
    if(RX2_Buffer[4] == 0x01) {
        printf("输出A");
    }

}

//任务2：蓝牙通讯模块 接收蓝牙模块从UART2传过来的消息,通过UART1发给PC
void task_UART2() _task_ BT_UART2{
    u8 i;
    while(1) {
        if(COM2.RX_TimeOut > 0) {
            //超时计数
            if(--COM2.RX_TimeOut == 0) {
                if(COM2.RX_Cnt > 0) {
                    //执行对应任务
                    do_control();

                    for(i=0; i<COM2.RX_Cnt; i++)	{
                        // RX2_Buffer[i]存的是接收的数据，写出用 TX2_write2buff
                        // TODO: 做具体的逻辑 on_uart2_recv
                        TX1_write2buff(RX2_Buffer[i]);
                    }
                }
                COM2.RX_Cnt = 0;
            }
        }
        os_wait2(K_TMO, 2);
    }
}

//任务3：超声波测距
void task_Ultrasonic() _task_ Ultrasonic{
    float distance;
    float distance_in_cm = -1;
    while(1) {
        if(Ultrasonic_get_distance(&distance) == 0) {
            distance_in_cm = distance;
            if(distance < 20) {
                printf("前方 %.2f cm有障碍物\n",distance);
                os_wait2(K_TMO,2);
                os_wait2(K_TMO,2);
            }
        } else {
            printf("测距失败！\n");
        }
        os_wait2(K_TMO, 2);
    }
}

//任务4： ADC电压检测
void task_ADC() _task_ ADC{

    while(1) {
        Battery_get_voltage(&voltage);
        if(voltage < 7.0) {
            printf("电源的电压值 %.2f 低于7V将自动关闭电源\n",voltage);
						
            os_wait2(K_TMO,10);
            os_wait2(K_TMO,10);
            os_wait2(K_TMO,10);
        }
        os_wait2(K_TMO, 200);
        os_wait2(K_TMO, 200);
    }
}
